//termsandconditions - Placing it away to not annoy
let controlnotfound = `Welcome to Control Not Found - The Game!
These terms and conditions outline the rules and regulations for the use of the Control Not Found game.
By accessing this website we assume you accept these terms and conditions. Do not continue to use Control Not Found if you do not agree to take all of the terms and conditions stated on this page. It does not matter if you agree to them because you will not read any further. You do not care about the terms and just want to play the game. 
We employ the use of cookies. By accessing Control Not Found, you agreed to use cookies in agreement with the ControlNotFoundGame.com’s Privacy Policy.
Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies. We will capture all the data that you provide once you accept these terms, and you agree to give us the right to use the data.
When you enter this website and play the game, you will give us access to your camera, and we will always be able to see you and what you do. Your presence on this website will not be private as well as every website you will visit. We will be able to follow your every move on the web. 
By playing this game, you will surrender your power and will not be in control of the gameplay.  You will not be able to win, no matter how you play it. You will not be in control. ControlNotFoundGame.com will have complete power over you.
`


//Variables
let textplacement = 30
let videocapture;
let hungersanity = 60
let maxhungersanity = 100
let socialsanity = 60
let maxsocialsanity = 100
let exercisesanity = 60
let maxexercisesanity = 100
let hygienesanity = 60
let maxhygienesanity = 100
let rectwidth = 200;
let timetilldeath
let maxtimetilldeath
let button
let hungerbuttonplus
let hungerbuttonminus
let socialbuttonplus
let socialbuttonminus
let exercisebuttonplus
let exercisebuttonminus
let hygienebuttonplus
let hygienebuttonminus
let yes
let timerhealthloss
let eventtimerhunger
let eventtimerhygiene
let eventtimersocial
let eventtimerexercise
let r
let g
let b
let r2
let g2
let b2
let r3
let g3
let b3
let r4
let g4
let b4
let you
let hungerimg;
let socialimg;
let exerciseimg;
let hygeineimg;
let retrofont
let startbutton;

// loading mario alike character
function preload() {

   retrofont = loadFont('assets/Retro Gaming.ttf');

  you = loadImage('assets/gamenohead.png')

  hungerimg = loadImage('assets/hungerimg.png')
  socialimg = loadImage('assets/socialimg.png')
  exerciseimg = loadImage('assets/dumbbell1.png')
  hygeineimg = loadImage('assets/hygeineimg.png')


}

//creating the initial screen, including the first button referring to lowest function in the code.
function setup() {

let col = color(255,255,234)
createCanvas(800,800)
background('#89cff0')
video = createCapture(VIDEO); //not yet implemented
video.hide(); //not yet implemented
scale(3);
video.size(360, 520);

button = createImg('assets/startwithblue.PNG');

button.style('background-color', col);
button.position(width/3, height/2);
button.mouseClicked(nextstate)

// The defining of all different button, all hidden till nextstate is run
//hunger button
hungerbuttonplus = createImg('assets/help.PNG')
hungerbuttonplus.style('background-color', col);
hungerbuttonplus.position(40, 100);
hungerbuttonplus.mouseClicked(givehunger)
hungerbuttonplus.hide()

//hungerhurt button
hungerbuttonminus = createImg('assets/hurt.PNG')
hungerbuttonminus.style('background-color', col);
hungerbuttonminus.position(40, 125);
hungerbuttonminus.mouseClicked(healthloser)
hungerbuttonminus.hide()

//exercise button
exercisebuttonplus = createImg('assets/help.PNG')
exercisebuttonplus.style('background-color', col);
exercisebuttonplus.position(40, 602);
exercisebuttonplus.mouseClicked(givesocial)
exercisebuttonplus.hide()

//exercisehurt button
exercisebuttonminus = createImg('assets/hurt.PNG')
exercisebuttonminus.style('background-color', col);
exercisebuttonminus.position(40, 627);
exercisebuttonminus.mouseClicked(socialloser)
exercisebuttonminus.hide()

//social button
socialbuttonplus = createImg('assets/help.PNG')
socialbuttonplus.style('background-color', col);
socialbuttonplus.position(720, 100);
socialbuttonplus.mouseClicked(giveexercise)
socialbuttonplus.hide()

//social hurt button
socialbuttonminus = createImg('assets/hurt.PNG')
socialbuttonminus.style('background-color', col);
socialbuttonminus.position(720, 125);
socialbuttonminus.mouseClicked(exerciseloser)
socialbuttonminus.hide()

//hygeine button
hygienebuttonplus = createImg('assets/help.PNG')
hygienebuttonplus.style('background-color', col);
hygienebuttonplus.position(720, 602);
hygienebuttonplus.mouseClicked(givehygiene)
hygienebuttonplus.hide();


//hygeine hurt button
hygienebuttonminus = createImg('assets/hurt.PNG')
hygienebuttonminus.style('background-color', col);
hygienebuttonminus.position(720, 627);
hygienebuttonminus.mouseClicked(hygieneloser)
hygienebuttonminus.hide();


}

function draw() {
//using rgb so it can be different colors later
console.log(yes)
r = 0
g = 235
b = 0

  //header
  push();
  fill('black');
  textFont(retrofont);
  textAlign(CENTER)
  textSize(30);
  text('Control Not Found', 400, 50);
  pop();

//check if timetilldeath has been called
if (timetilldeath === '1') {
  if (hungersanity === 0) {
    maxtimetilldeath = "27 seconds"
  }

  if (hungersanity === 20) {
    maxtimetilldeath = "24 seconds"
  }

  if (hungersanity === 60) {
    maxtimetilldeath = "21 seconds"
  }
  if (hungersanity === 80) {
    maxtimetilldeath = "18 seconds"
  }
  if (hungersanity === 100) {
    maxtimetilldeath = "15 seconds"
  }
  if (hungersanity === 120) {
    maxtimetilldeath = "12 seconds"
  }
  if (hungersanity === 140) {
    maxtimetilldeath = "9 seconds"
  }
  if (hungersanity === 160) {
    maxtimetilldeath = "6 seconds"
  }
  if (hungersanity === 180) {
    maxtimetilldeath = "3 seconds"
  }
  if (hungersanity === 200) {
    maxtimetilldeath = "0 seconds"
  }
  push();
  fill("#89cff0")
  rect(80,760,290,30)
  pop();
  push();
  fill(0)
  textSize(13)
  text("  Max time till death " + maxtimetilldeath,80,780)
  pop();
  }
// fix code so it only runs when nextstate has been called. Check if its true.
// tried typeof with definition - but it only refer to a variable, and im guessing this function is always valid.
if (yes === '1') {

//webcam for the character

  image(video, 210, 170);


//pixel body - Character
  push();

  imageMode(CENTER);
  you.resize(850,480);
  image(you,400,320)

  pop();

  push();
 //icons

  //hunger icon
   hungerimg.resize(30, 30);
  image(hungerimg, 105, 65);

  //social icon
   socialimg.resize(23,23);
  image(socialimg, 505, 70);

  //exercise icon
   exerciseimg.resize(25,25);
  image(exerciseimg, 105, 570);

  //hygeine icon
  hygeineimg.resize(27,27);
  image(hygeineimg, 505, 567);
(pop);

  //headers for each bar

  //hunger bar
  fill('black');
  textFont(retrofont);
  textSize(15);
  text('Hunger', 140, 90);

  //social bar
   textSize(15);
  text('Social', 535, 90);

  //exercise bar
  textSize(15);
  text('Exercise', 140, 587);

  //hygeine bar
  textSize(15);
  text('Hygeine', 535, 587);



   // hungerhealthbar - color changer - then drawing the square
  if (hungersanity >= 100 && hungersanity <= 140) {
     r = 235
     g = 235
     b = 0
  }

  if (hungersanity >= 160 && hungersanity <= 200) {
     r = 235
     g = 0
     b = 0
  }
   //Bar one for health
  push();
  fill(r,g,b)
  strokeWeight(2)
  rect(100,100,rectwidth,50)
  pop();
 // bar two for healthbar
push();
   noStroke()
   fill('#89cff0')
   rect(100,100,hungersanity,50);
pop();

//Game Ender for hunger
   if (hungersanity === 200) {
     fill(235,0,0)
     textSize(62)
     textAlign(CENTER);
     push();
     fill("#001B8A")
      text("GAME OVER",400,400)
      pop();
     noLoop()

   }
  //this will replicate itself for each representative health indicator
 //Limit for max sanity - using square to fake background color, since it doesnt loop canvas
   if (socialsanity === -20) {
     socialsanity = socialsanity + 20
     push()
     noStroke()
     fill('#89cff0')
     rect(60,80,40,80)
     pop();

   }
// Social sanity healthbar
   if (socialsanity >= 100 && socialsanity <= 140) {
      r2 = 235
      g2 = 235
      b2 = 0
   }

   if (socialsanity >= 160 && socialsanity <= 200) {
      r2 = 235
      g2 = 0
      b2 = 0
   }

   if (socialsanity >= 0 && socialsanity <= 100) {
      r2 = 0
      g2 = 235
      b2 = 0
   }

   push();
   fill(r2,g2,b2)
   strokeWeight(2)
   rect(100,600,rectwidth,50)
   pop();

   push();
    noStroke()
    fill('#89cff0')
    rect(100,600,socialsanity,50);
    pop();

   //Game Ender
    if (socialsanity === 200) {
      fill(235,0,0)
      textSize(62)
      textAlign(CENTER);
      push();
      fill("#001B8A")
      text("GAME OVER",400,400)
      pop();
      noLoop()

    }
   //Limit for max sanity - using square to fake background color, since it doesnt loop canvas
    if (socialsanity === -20) {
      socialsanity = socialsanity + 20
      push()
      noStroke()
      fill('#89cff0')
      rect(60,80,40,80)
      pop();
    }

    if (socialsanity === 200) {
      fill(235,0,0)
      textSize(62)
      textAlign(CENTER);
      push();
      fill("#001B8A")
      text("GAME OVER",400,400)
      pop();
      noLoop()
}

// exercise sanity healthbar
  if (exercisesanity >= 100 && exercisesanity <= 140) {
     r3 = 235
     g3 = 235
     b3 = 0
  }

  if (exercisesanity >= 160 && exercisesanity <= 200) {
     r3 = 235
     g3 = 0
     b3 = 0
  }

  if (exercisesanity >= 0 && exercisesanity <= 100) {
     r3 = 0
     g3 = 235
     b3 = 0
  }

  push();
  fill(r3,g3,b3)
  strokeWeight(2)
  rect(500,100,rectwidth,50)
  pop();

  push();
   noStroke()
   fill('#89cff0')
   rect(500,100,exercisesanity,50);
   pop();


  //Game Ender
   if (exercisesanity === 200) {
     fill(235,0,0)
     textSize(62)
     textAlign(CENTER);
     push();
     fill("#001B8A")
      text("GAME OVER",400,400)
      pop();
     noLoop()

   }

   // hygiene sanity healthbar
     if (hygienesanity >= 100 && hygienesanity <= 140) {
        r4 = 235
        g4 = 235
        b4 = 0
     }

     if (hygienesanity >= 160 && hygienesanity <= 200) {
        r4 = 235
        g4 = 0
        b4 = 0
     }

     if (hygienesanity >= 0 && hygienesanity <= 100) {
        r4 = 0
        g4 = 235
        b4 = 0
     }

     push();
     fill(r4,g4,b4)
     strokeWeight(2)
     rect(500,600,rectwidth,50)
     pop();

     push();
      noStroke()
      fill('#89cff0')
      rect(500,600,hygienesanity,50);
      pop();

     //Game Ender
      if (hygienesanity === 200) {
        fill(235,0,0)
        textSize(62)
         textAlign(CENTER);
         push();
         fill("#001B8A")
        text("GAME OVER",400,400)
        pop();
        noLoop()

      }
     //Limit for max sanity - using square to fake background color, since it doesnt loop canvas
      if (hygienesanity === -20) {
        hygienesanity = hygienesanity + 20
        push()
        noStroke()
        fill('#89cff0')
        rect(60,80,40,80)
        pop();
      }

      if (hygienesanity === 200) {
        fill(235,0,0)
        textSize(62)
        textAlign(CENTER);
        push();
        fill("#001B8A")
      text("GAME OVER",400,400)
      pop();
        noLoop()
   }
}
}

// Nextstate, is the state after which button has been clicked.
// that is the reason for why it runs show for all buttons in second screen.
// it clear the canvas as well or it actually just paints over it.
// yes = '1' is the check for whether the function has been run
//
function nextstate() {
   if (confirm(controlnotfound)) {
     createCanvas(800,800)
     background('#89cff0')
     button.hide()
     yes = '1'
     // the 4 buttons
    // hunger - social - exercise - hygiene
    // show functions
     hungerbuttonplus.show();
     socialbuttonplus.show();
     exercisebuttonplus.show();
     hygienebuttonplus.show();

//
     //hungersanity timer + button functions - this is also replicated
     //3500 is ms, so 3.5 seconds
     timerhealthloss = setInterval(healthloser, 3000);
     timerhealthloss = setInterval(socialloser, 3000);
     timerhealthloss = setInterval(exerciseloser, 3000);
     timerhealthloss = setInterval(hygieneloser, 3000);

     //event timers
     eventtimerhunger = setTimeout(hunger, 10000);
     eventtimerhygiene = setInterval(hygiene, 12000);
     eventtimersocial = setInterval(social, 14000);
     eventtimerexercise = setInterval(exercise, 16000);
   }
}

//interaction with the buttons - replicates itself for all 8 buttons
function givehunger() {
 hungersanity = hungersanity - 20
}

function healthloser() {
  hungersanity = hungersanity + 20

}

function hunger() {
  hungerbuttonminus.show();
  hungerbuttonplus.hide();
  push();
  fill(0)
  textSize(13)
  text("We have removed your access to help, as you have been deemed unfit",80,750)
  pop();
  timetilldeath = '1'



}


function hygiene() {
  hygienebuttonminus.show();
  hygienebuttonplus.hide();
}

function social() {
  socialbuttonminus.show();
  socialbuttonplus.hide();
}

function exercise() {
  exercisebuttonminus.show();
  exercisebuttonplus.hide();
}


function givesocial() {
socialsanity = socialsanity - 20
}

function socialloser() {
socialsanity = socialsanity + 20

}

function giveexercise() {
exercisesanity = exercisesanity - 20
}

function exerciseloser() {
exercisesanity = exercisesanity + 20

}

function givehygiene() {
hygienesanity = hygienesanity - 20
}

function hygieneloser() {
hygienesanity = hygienesanity + 20

}
