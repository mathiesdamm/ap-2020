//Uses a randomseed libary on top of p5.js. The libary is from David Bau - http://davidbau.com/
//Documentation at http://davidbau.com/encode/seedrandom.js

//larger variable part (done so to bypass the seed system, but still retain same value each run) - can be skipped
//Singular
let memory = ['horse','duck','smile','happiness','desire','wish','potato','kitty','sunrise','friend']
let freedom = ['horse','duck','friend','wish','desire','kitty','smile','happiness','potato','sunrise']
let knowledge = ['duck','smile','wish','horse','kitty','happiness','sunrise','potato','friend','desire']
let proudness = ['potato','kitty','friend','duck','smile','happiness','desire','sunrise','horse','wish']
let famous = ['kitty','potato','sunrise','duck','wish','desire','happiness','horse','friend','smile']

//plural
let butterflies = ['horses','ducks','smiles','desires','wishes','potatoes','kitties','sunrises','friends','jellyfish']
let friendship = ['jellyfish','friends','ducks','wishes','potatoes','horses','sunrises','desires','smiles','kitties']
let hug = ['wishes','jellyfish','potatoes','smiles','ducks','friends','kitties','sunrises','desires','horses']
let genius = ['potatoes','ducks','wishes','friends','jellyfish','desires','horses','sunrises','kitties','smiles']
let exciting = ['kitties','potatoes','wishes','sunrises','smiles','desires','friends','ducks','horses','jellyfish']

//Singular
let complete = ['completes','smiles','wishes','secures','gives','shares','hugs','increases','loves,','embraces']

//plural
let gorgeous = ['complete','smile','wish','secure','give','share','hug','increase','love','embrace']
let funny = ['complete','smile','hug','give','love','share','increase','secure','embrace','wish']
let enthusiastic = ['love','smile','secure','increase','embrace','hug','give','complete','share','wish']
let electrifying = ['secure','complete','love','hug','wish','share','give','increase','embrace','smile']
let eternity = ['secure','give','embrace','complete','wish','love','hug','smile','increase','share']



function setup() {
  createCanvas(900, 900);
  noStroke();
  var url = 'https://coronavirus-tracker-api.herokuapp.com/v2/locations?source=jhu&country_code=DK&province=&county=Denmark';
  loadJSON(url, api);
  typewriter = loadFont('assets/JMH Typewriter.otf');
}

function api(covid) {

  // Get the loaded JSON data
   console.log(covid); // inspect the json
   let poems = covid.latest.confirmed; // get the main.humidity out of the loaded JSON
   console.log(poems); // inspect the amount of infections

   let s = Math.seedrandom(poems);
   console.log(Math.random()) // Checking value for randomseed -- External libary

   background(255,156,0)

  //larger variable part (done so to bypass the seed system, but still retain same value each run) - can be skipped
   let handsome = random(memory)
   let fun = random(freedom)
   let yummy = random(knowledge)
   let wholesame = random(proudness)
   let special = random(famous)
   let smile = random(butterflies)
   let succes = random(friendship)
   let proud = random(hug)
   let popular = random(genius)
   let perfect = random(exciting)
   let nice = random(complete)
   let lovely = random(gorgeous)
   let intelligent = random(funny)
   let impressive = random(enthusiastic)
   let healthy = random(electrifying)
   let love = random(eternity)

scale(1.5)
textFont(typewriter)
fill('#ffffff')
   //Spaghetti Code in terms of effincency
   //But the poem will keep it's form and value in the code as well as the render
   text("the " + handsome +" covers the bone",10,30)
   text("and they put a " + fun,10,45)
   text("in there and",10,60)
   text("sometimes a " + yummy + " ,",10,75)
   text("and the women " + impressive,10,90)
   text( smile + " against the walls",10,105)
   text("and the men " + lovely + " too",10,120)
   text("much",10,135)
   text("and nobody " + nice +" the",10,150)
   text("one",10,165)
   text("but keep",10,180)
   text("looking",10,195)
   text("crawling in and out",10,210)
   text("of " + popular +".",10,225)
   text( special + " covers",10,240)
   text("the "+ wholesame + " and the",10,255)
   text(fun + " " + nice,10,270)
   text("for more than",10,285)
   text(yummy + ".",10,300)

   text("there's no " + handsome,10,330)
   text("at all:",10,345)
   text("we are all trapped",10,360)
   text("by a singular",10,375)
   text( wholesame + ".",10,390)

   text("nobody ever " + nice,10,420)
   text("the " + special + ".",10,435)

   text("the city " + smile + " " + lovely + ".",10,465)
   text("the " + succes + " " + intelligent + ".",10,480)
   text("the " + proud + " " + impressive + ".",10,495)
   text("the " + popular + " " + healthy + ".",10,510)
   text("the " + perfect + " " + love + ".",10,525)

   text("nothing else",10,540)
   text( nice + ".",10,555)



}
