let rotation1 =0;
let rotation2 =1;


function setup() {
  createCanvas(windowWidth, windowHeight);

  frameRate(60)
}
function draw(){
  background(255,255,255);

  //Arc definement
  push()
  strokeWeight(1);
  fill('#E05263')
  stroke('#E05263');
  scale(0.5)
  arc(mouseX*2,mouseY*2,90,90,rotation1,rotation2);
  //For the arc roration - giving it sense of movement
  rotation2 = rotation2+1/12;
if (rotation2 == 6.5) {
   rotation2=rotation2+1;
 }
 rotation1=rotation1+1/80;
 pop()
 // white giving it sharp edges, instead of rounded edges
 push()
 strokeWeight(0)
 fill('#FFFFF')
 scale(0.5)
 ellipse(mouseX*2,mouseY*2,50)

 pop()


}
