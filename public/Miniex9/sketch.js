//some code was used by emoji machine by boysonhudson
// source : https://editor.p5js.org/boysonhudson/sketches
// code was heavily modified to fit our project, and we introduced several new concepts
// Program cannot do more than one request per load - so youll need to reload to get different results
// Program made by Amanda Hansen - Caroline Kreutz - Katrine Holm Kjærgaard - Mathies Damm-Henrichsen
// https://gitlab.com/amanda.hansen1404
// https://gitlab.com/carolinekreutz
// https://gitlab.com/KatrineHK
// https://gitlab.com/mathiesdamm

// variable defining - had to use alot due to using 4 api's
var button1;
var button2;
var button3;
var button4;
var img1;
var img2;
var img3;
var img4;
let thefact;
let setupgeneral;
let punchlinegeneral;
let setupprogramming;
let punchlineprogramming;
let setupknockknock;
let punchlineknockknock;
var url = 'https://official-joke-api.appspot.com/jokes/general/random';
var url2 = 'https://uselessfacts.jsph.pl/random.json?language=en';
var url3 = 'https://official-joke-api.appspot.com/jokes/programming/random';
var url4 = 'https://official-joke-api.appspot.com/jokes/knock-knock/random';

//loading images for buttons
//Wasnt necassary to use loadjson here, as it can be called in setup instead
function preload() {
  img1 = loadImage('emoji/facts.png');
  img2 = loadImage('emoji/general.png');
  img3 = loadImage('emoji/knockknock.png');
  img4 = loadImage('emoji/Programming.png');

}



function setup() {
  createCanvas(windowWidth, windowHeight);
  background(220,170,180)
//emoji implementation
imageMode(CENTER);
button1 = new Button(windowWidth/2+25, windowHeight/1.6, 40, 40, img1);
button2 = new Button(windowWidth/2+75, windowHeight/1.6, 40, 40, img2);
button3 = new Button(windowWidth/2-25, windowHeight/1.6, 40, 40, img3);
button4 = new Button(windowWidth/2-75, windowHeight/1.6, 40, 40, img4);


  //api implementation
  loadJSON(url,generaljoke);
  loadJSON(url2,uselessfacts);
  loadJSON(url3,programmingjoke);
  loadJSON(url4,knockknockjoke);
}


// Callback for each api call - setting the json to global variable
// F.X setupgeneral becomes equal to [0].setup json of url1
// Making it a global variable allows us to call it later outside the function
// You do however have to make a global variable to refer to = setupgeneral in this case

function generaljoke(nr1) {
  console.log(nr1)
  setupgeneral = nr1[0].setup;
  punchlinegeneral = nr1[0].punchline;
//  console.log(setupgeneral)

  }

function uselessfacts(fact) {
//  console.log(fact)
  thefact = fact.text;
//  console.log(thefact)
  }

function programmingjoke(nr2) {
  //console.log(nr2)
  setupprogramming = nr2[0].setup;
  punchlineprogramming = nr2[0].punchline;

  }

function knockknockjoke(nr3) {
  //console.log(nr3)
  setupknockknock = nr3[0].setup;
  punchlineknockknock = nr3[0].punchline;
console.log(setupknockknock)
  }




//draw of button - continusloy drawn to do animation on click.
function draw() {

 button1.display()
 button2.display()
 button3.display()
 button4.display()


}



//interaction - The && functions is the mapping of the click - included in button.js
// This was is done due to button being a object that is scaling with "movement"
// I check for asynchronius load, because the code is run faster than the api fetches data
// F.X If(thefact) is used to check if json data is loaded before running the rest, enabling it to have interaction
// kind of clumsy way of inplementing it, but to our knowledge it is the only way to do this in js

function mouseClicked () {
  if ((mouseX > button1.x-button1.sizeWidth/2)
  && (mouseX < button1.x + button1.sizeWidth/2)
  && (mouseY > button1.y-button1.sizeHeight/2)
  && (mouseY < button1.y + button1.sizeHeight/2)){
    if (thefact) {
      background(220,170,180)
      textSize(20)
      text(thefact,10,30)
      console.log(thefact)

    }
  }
  else if ((mouseX > button2.x-button2.sizeWidth/2)
  && (mouseX < button2.x + button2.sizeWidth/2)
  && (mouseY > button2.y-button2.sizeHeight/2)
  && (mouseY < button2.y + button2.sizeHeight/2)){
    if (setupgeneral) {
      background(220,170,180)
      textSize(20)
      text(setupgeneral,windowWidth/6,30)
      text(punchlinegeneral,windowWidth/6,90)
  }
  }
  else if ((mouseX > button3.x-button3.sizeWidth/2)
  && (mouseX < button3.x + button3.sizeWidth/2)
  && (mouseY > button3.y-button3.sizeHeight/2)
  && (mouseY < button3.y + button3.sizeHeight/2)){
    if (setupknockknock) {
      background(220,170,180)
      textSize(20)
      text(setupknockknock,windowWidth/6,30)
      text(punchlineknockknock,windowWidth/6,180)
  }
  }
  else if ((mouseX > button4.x-button4.sizeWidth/2)
  && (mouseX < button4.x + button4.sizeWidth/2)
  && (mouseY > button4.y-button4.sizeHeight/2)
  && (mouseY < button4.y + button4.sizeHeight/2)){
    if (setupprogramming) {
      background(220,170,180)
      textSize(20)
      text(setupprogramming,windowWidth/6,30)
      text(punchlineprogramming,windowWidth/6,90)
  }
  }
  }
