
let ctracker;
var coverup;

function preload() {
    img = loadImage('Assets/error.png');
}


function setup() {
  background(100);
  img.filter(GRAY)
  img.filter(POSTERIZE, 2 + random(200));
 img.filter(DILATE);
 img.filter(ERODE);

  coverup = ellipse(50,50,50,50)

  //web cam capture
  let capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.position(0,0);
  //capture.hide();
  let c = createCanvas(640, 480);
  c.position(0,0);


  //setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);


}

function mousePressed() {
  clear();
}



function draw() {
  let positions = ctracker.getCurrentPosition();

  if (positions.length) { //check the availability of web cam tracking
    for (let i=0; i<positions.length; i++) {  //loop through all major face track points (see: https://www.auduno.com/clmtrackr/docs/reference.html)
      clear();
        image(img,positions[60][0]-100,positions[60][1]-80, 200, 115)
        image(img,positions[29][0]-100,positions[29][1]-80, 200, 115)
       image(img,positions[24][0]-100,positions[24][1]-80, 200, 115)

    }
  }
}
