**FlowChart 1 - Individual API:**
I think the idea of Miniex5, was to remind myself of earlier program while still challenging myself to new stuff.
I challenged myself to using an api to solve my idea. The flowchart seem quite simple and straightforward, which I actually prefer.
Coming from STEM field, I know of nothing worse than using more than what is actually needed. I felt as if i challenged myself, and managed to keep the code very clean.
![](https://i.imgur.com/0EFuqnw.png)




**Descriptions of our ideas and the flowcharts:**

**Idea number 1 - Movie API:**

![](https://gitlab.com/carolinekreutz/ap2020/-/raw/master/public/mx10/movieflowchart.png)

Our first idea is a program that utilises a Movie API to help users learn more about a specific movie. The idea is that the user types in a movie they are considering and therefore need to know more about - and with the help of an API, a review and description of the chosen movie will pop up. The user will therefore quickly learn more about a movie they have seen on a streaming service, for example, before actually watching. The aim is: if the user doesn&#39;t like the information that the API provides on the movie, they won&#39;t waste time by starting to watch it. This idea is inspired by our own &quot;problem&quot; of rarely knowing what is actually worth watching when browsing Netflix or another streaming service - you often waste time by starting a movie and closing it down before finishing it because you don&#39;t like it.

We are aware of the fact that our flowchart is missing some elements that might actually be a part of a source code that will make our idea for this program executable. However, we found it difficult to create a fully accurate flowchart of a program we had not written as we could only image how it might be written. While some elements might be missing, perhaps even some crucial elements, we focused more on learning how to use the flowchart and the purpose of creating one. Lastly, we have realised that we did not need to create such a technical flowchart with details of the code, but we could have created a more conceptual flowchart of the idea for the program. Despite this, creating the flowchart forced us to critically think more about the technical possibilities and limitations of the potential program. Furthermore, we have learnt from the process and gained experience with flowcharts.

**Flowchart #2 - Loss of user control:**

![](https://gitlab.com/carolinekreutz/ap2020/-/raw/master/public/mx10/gameflowchart.png)

Game of controlling a tamagotchi like character, with the help of an AI. But as the game progresses (can be counted in framecount), the AI disables/ignores the users input, and instead gives the player a popup warning, letting him/her know that it has taken control, to perform what it deems ideal choices.

Our second idea was to create a Tamagotchi-inspired game where you start of by using it like you normally would - by you taking care of the character. However, after some time, the artificial intelligence behind the program will take control over the program. Thereby, the program will make sure the user is out of control and will not able to do what they want to do with it. Also, this adds a bit of chaos to the user as it is never a nice feeling to not be in control of what you want to do. In order to ensure that the user knows they aren&#39;t in control anymore, an alert will pop up, which tells you that the AI has taken over and that the user can no longer be in charge of the happenings of the &quot;Tamagotchi&quot;. We wanted to do something where the user gets scared of not being in control to highlight the fact that, once you post something online, what happens to it is out of your hands. Also we liked the fact about an alert popping up to warn the user as it creates the element of surprise.

As with creating the flowchart for the first idea, we also unsure of what to include in the flowchart. We didn&#39;t know if we had to create a fully technical flowchart, a flowchart that is both conceptual and technical, or just a conceptual flowchart. This is especially difficult when we haven&#39;t written the code - so how do we create a technical flowchart without knowing all of the elements that are required to run an executable program? Our doubts around this will be apparent in the completed flowchart - some elements will be missing, and some elements might be too simple or too technical. After already creating the two flowcharts, we realised they were supposed to be conceptual rather than technical. We decided not to make two new and perfected versions as we felt they were both technical and conceptual - and still communicated our idea.


**Readme group**
- What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

We found it quite difficult to figure out the connection between what we needed to communicate and how we had to do it. As we haven&#39;t done this before, knowing exactly how to facilitate our ideas through a flowchart using only a few words was quite the challenge. How do you communicate exactly what you want? Especially if you don&#39;t know what is truly possible and impossible to code and to create an executable program? It was difficult to decide which elements were crucial to include to create an understandable flowchart, and which elements would make the flowchart redundant. This experience of creating the two flowcharts really made the connection of simplicity at the level of communication and complexity at the level of algorithmic procedure clear. We have learnt how challenging it is to simplify the program code/algorithmic procedure and communicate it through the flowchart. We are still unsure of how to do this properly - but you can always imagine what _you_ want to know when reading a flowchart and go from there.

Lastly, as mentioned above, we found it challenging to create a flowchart of the source code for a programmer as opposed to creating a more conceptual flowchart of the program for the user. The last one would have been easier to do as you only need to express an idea rather than imagining the code that lies behind it. We realised later that a conceptual flowchart would have sufficed - and we did not need to make it so code-based. As mentioned before, we still gained a lot from the process, and this is the most important part.

- What are the technical challenges for the two ideas and how are you going to address them?

We had a hard time coming up with these two ideas which made them a bit more rushed than we would have preferred. We do not believe that one of these two ideas will be the one we use for the final project as they are not complex or quite exciting enough. We imagine our final idea will be more focused on making a difference or more provocative. We don&#39;t want it to just be fun and games but it needs to actual say something and mean something.

We will most likely use our experience with creating the flowcharts for these ideas + the idea of data capture and loss of control for the final project. The technical challenges are therefore not entirely clear, but we are very aware of the conceptual challenge - of coming up with a idea we can be passionate about.

- What is the value of (the individual and) the group flowchart that you have produced?

Making flowcharts both individually and in the group helped us understand flowcharts better and how they can present different aspects of a program. It was definitely different making the flowchart in the group because we all saw many different aspects of these &quot;fictional&quot; programs we imagined. We tried combining the technical/code aspect and the conceptual aspect of both programs in order for the reader of the flowchart to understand what the program is able to do and how. We have realised that the flowchart indeed is a tool for critical thinking - it allowed us to view our program ideas from different perspectives and made us realise our own limitations and skills as well as the possibilities and limitations of our ideas. We can definitely use this experience for when we use flowcharts again - and thereby improve ourselves. As mentioned in &quot;The multiple meanings of a flowchart&quot;, flowcharts allow us to see software, and in this case the program ideas, in different ways that seemed impossible before.

**Readme individual**
- What are the challenges between simplicity at the level of communication and complexity at the level of algorithmic procedure?

In the invidual flowchart, I feel the idea between creating something simple to understand and coding something complex, goes hand in hand. For most projects I would prefer a flowchart where it makes sense, what goes where. However as alot of our program are limited by time, I feel the need to create spaghetti code is increasing, thus making it harder to make a simple flowchart.
- What are the technical challenges for the two ideas and how are you going to address them?

I had worked with api's beforehand, but I found it to understand the syntax from js + p5. My experience was from web exploitation, so I was only familiar with the technical aspect, and not with how to actually use it.
But to understand the syntax just required to find some documentation and start reading.

- What is the value of (the individual and) the group flowchart that you have produced?

To explain my program to something like my family or a potential client - I think a flowchart would do well of showing both a good explanation of the conceptual idea and a easy to understand technical aspect of it.
For myself, I often set my code up in my own preferred syntax, and conceptualise the connection by writing on paper. 
